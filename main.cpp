/*
Author: Sean Richardson : N3010267
Date:   3/12/2013
Version: 1.0.0
*/

#include <prg_core.hpp>
#include <iostream>
#include "ImageConverter.hpp"
#include <fstream>


using namespace std;
using namespace prg;

uint w;
uint h;

PIXEL* pixels;
long pixelCount = 0;

bool mainMenu();
void optionsMenu();
void editMenu();
void monochromeMenu();
void rotateMenu();
void clockwiseMenu();
void counterClockwiseMenu();

/*
 * This program allows a user to open an image save that image
 * and edit that image
 */
int main()
{
    bool exitMenu = false;
    do
    {
        exitMenu = mainMenu();
    }
    while(exitMenu == false);
    return 0;
}

bool mainMenu()
{
    // declare local variables
    bool exitMenu = false;
    string filename;
    Image originalImage;
    int menuOption = 0;



    cout << "********* Main Menu *********" << endl;
    cout << "1) Open File" << endl;
    cout << "2) Exit" << endl;
    cin >> menuOption;

    switch(menuOption)
    {
    case 1:
    {
        //read filename into a variable
        cout << "Enter File Name: " << endl;
        cin >> filename;

        pixels = convertFromImage(filename, w, h, pixelCount);

        if(pixels != nullptr)
        {
            //display options menu
            optionsMenu();

            cout << "Enter save filename: ";

            string outputFilename;

            cin >> outputFilename;

            convertToImage(w, h, pixels, outputFilename);

            //free up memory
            delete[] pixels;
        }
    }
    break;
    case 2:
        exitMenu = true;
        break;
    default:
        mainMenu();
    }
    return exitMenu;
}
void optionsMenu()
{
    int menuOption = 0;
    cout << "********* Options Menu *********" << endl;
    cout << "1) Edit file" << endl;
    cout << "2) Save File As" << endl;
    cin >> menuOption;

    switch(menuOption)
    {
    case 1:
        editMenu();
        break;
    case 2:
        break;
    default:
        cerr << "Option Does Not Exist" << endl;
        optionsMenu();
    }

}


void editMenu()
{
    int menuOption = 0;
    cout << "********* Edit **********" << endl;
    cout << "1) Monochrome" << endl;
    cout << "2) Greyscale" << endl;
    cout << "3) Rotate" << endl;
    cout << "4) Go Back" << endl;
    cin >> menuOption;

    switch(menuOption)
    {
    case 1:
        monochromeMenu();
        break;
    case 2:
        greyScale(&pixels[0], pixelCount);
        break;
    case 3:
        rotateMenu();
        break;
    case 4:
        optionsMenu();
        break;
    default:
        cerr << "Option Does Not Exist" << endl;
        editMenu();

    }
}

void monochromeMenu()
{
    int menuOption = 0;
    cout << "********* Monochrome Menu *********" << endl;
    cout << "1) Red Monochrome" << endl;
    cout << "2) Green Monochrome" << endl;
    cout << "3) Blue Monochrome" << endl;
    cout << "4) Go Back" << endl;
    cin >> menuOption;

    switch(menuOption)
    {
    case 1:
        redMonochrome(&pixels[0], pixelCount);
        break;
    case 2:
        greenMonochrome(&pixels[0], pixelCount);
        break;
    case 3:
        blueMonochrome(&pixels[0], pixelCount);
        break;
    case 4:
        editMenu();
        break;
    default:
        cerr << "Option Does Not Exist" << endl;
        monochromeMenu();
    }
}

void rotateMenu()
{
    int menuOption = 0;
    cout << "********* Rotate Menu *********" << endl;
    cout << "1) Clockwise" << endl;
    cout << "2) Counter-Clockwise" << endl;
    cin >> menuOption;

    switch(menuOption)
    {
    case 1:
        clockwiseMenu();
        break;
    case 2:
        counterClockwiseMenu();
        break;
    default:
        cerr << "Option Does Not Exist" << endl;
        rotateMenu();
    }
}

void clockwiseMenu()
{
    int menuOption = 0;
    cout << "********* Clockwise Menu *********" << endl;
    cout << "1) 90  Degrees" << endl;
    cout << "2) 180 Degrees" << endl;
    cout << "3) 270 Degrees" << endl;
    cin>> menuOption;

    switch(menuOption)
    {
    case 1:
        rotation( pixels, pixelCount, h, w, CLOCKWISE, 90);
        break;
    case 2:
        rotation( pixels, pixelCount, h, w, CLOCKWISE, 180);
        break;
    case 3:
        rotation( pixels, pixelCount, h, w, CLOCKWISE, 270);
    default:
        cerr << "Option Does Not Exist" << endl;
        clockwiseMenu();
    }
}

void counterClockwiseMenu()
{
    int menuOption = 0;
    cout << "********* Counter-Clockwise Menu *********" << endl;
    cout << "1) 90  Degrees" << endl;
    cout << "2) 180 Degrees" << endl;
    cout << "3) 270 Degrees" << endl;
    cin>> menuOption;

    switch(menuOption)
    {
    case 1:
        rotation( pixels, pixelCount, h, w, COUNTER_CLOCKWISE, 90);
        break;
    case 2:
        rotation( pixels, pixelCount, h, w, COUNTER_CLOCKWISE, 180);
        break;
    case 3:
        rotation( pixels, pixelCount, h, w, COUNTER_CLOCKWISE, 270);
    default:
        cerr << "Option Does Not Exist" << endl;
        counterClockwiseMenu();
    }
}
