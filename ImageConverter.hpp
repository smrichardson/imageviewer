/*
Author: Sean Richardson : N3010267
Date:   3/12/2013
Version: 1.0.0
*/

#include <prg_core.hpp>
#include <string>

using namespace prg;
using namespace std;

#define CLOCKWISE 1
#define COUNTER_CLOCKWISE 2

// define the structure of a pixel
struct PIXEL {
    float r;
    float g;
    float b;
    float a;
};

// convert from prg image format
PIXEL* convertFromImage(string filename, uint& width, uint& height, long& pixelCount);
// convert to prg image format
void convertToImage(uint width, uint height, PIXEL* pixels, const string& filename);

// add a coloured filter to image
void redMonochrome(PIXEL* pixels, long pixelCount);
void greenMonochrome(PIXEL* pixels, long pixelCount);
void blueMonochrome(PIXEL* pixels, long pixelCount);

// change image to greyscale using Y = 0.2126R + 0.7152G + 0.0722B
void greyScale(PIXEL* pixels, long pixelCount);

// Rotate the Image by a rotation amount 90, 180, 270 in a direction
void rotation( PIXEL* original, long pixelCount, uint& height, uint& width, int directionRotated, int rotationAmount);
