/*
Author: Sean Richardson : N3010267
Date:   3/12/2013
Version: 1.0.0
*/

#include <prg_core.hpp>
#include "ImageConverter.hpp"
#include <iostream>
#include <string>

using namespace prg;

bool hasValidFileExtension(string filename);
string getExtension(string pathName);

/*
 * This function opens the image via the prg library
 * and stores it in memory for manipulation
 *
 * PARAMETERS
 * filename:   the name of the file to open, including extension e.g. amiga_500.bmp
 * width:      the width of the loaded image in pixels
 * height:     the height of the loaded image in pixels
 * pixelCount: the number of pixels in the loaded image
 *
 * RETURNS
 * PIXEL*:  pointer to the start of the memory that holds the image
 * nullptr: if filename does not exist
 */
PIXEL* convertFromImage(string filename, uint& width, uint& height, long& pixelCount)
{
    // holds the image loaded by the prg library
    Image img;

    // check file exists by trying to open it
    ifstream inputFile( filename );
    if ( !inputFile.is_open() )
        cerr << "Unable to open file" << endl;
    else
    {
        // we know file exists so close it
        inputFile.close();

        // use library to open file
        ImageFile(filename).load(img);

        // create internal storage for image to allow manipulation
        pixelCount = img.getPixelCount();
        PIXEL* pixels = new PIXEL[pixelCount];

        // get width and height of the original image in pixels
        width = img.getWidth();
        height = img.getHeight();

        // convert the image into pixel format
        // origin of image at bottom left
        // read image from left to right row by row
        // store in array of pixels
        for(uint y = 0; y < height; y++)
        {
            for(uint x = 0; x < width; x++)
            {
                // getting an individual pixel from the original image
                Colour colour = img.getPixel(x , y);

                // convert from RGB HEX to openGL colour format
                pixels[x + y * width].r = (int)colour.getR()/255.0;
                pixels[x + y * width].g = (int)colour.getG()/255.0;
                pixels[x + y * width].b = (int)colour.getB()/255.0;
                pixels[x + y * width].a = (int)colour.getA()/255.0;
            }
        }
        return pixels;
    }
    return nullptr;
}

/*
 * This function stores the manipulated image using the prg library
 *
 * PARAMETERS
 * filename:   the name of the file to open, including extension e.g. amiga_500.bmp
 * width:      the width of the image to save, im pixels
 * height:     the height of the image to save, in pixels
 */
void convertToImage(uint width, uint height, PIXEL* pixels, const string& filename)
{
    //find the start position of the file extension
    if( !hasValidFileExtension( filename ))
        cerr << "Must enter a valid file extension e.g. '.bmp/.tga'" << endl;
    else
    {
        // create a new image of height and width
        Image image { width , height};

        // loop through each row
        for(uint y = 0; y < height; y++)
        {
            // loop through each coloumn
            for(uint x = 0; x < width; x++)
            {
                // get the pixel from the array
                PIXEL pixel = pixels[x + y * width];

                // create new colour object to represent the pixel( using RGBA HEX Values )
                Colour colour { (byte)(pixel.r * 255), (byte)(pixel.g * 255), (byte)(pixel.b * 255), (byte)(pixel.a * 255) };

                // store pixel at correct co-ordinate on the prg image
                image.setPixel( x , y, colour);

            }
        }
        // save the prg image to filename including extension
        ImageFile { filename } .save( image );
    }
}

/* This function adds a red filter to the image
 *
 * PARAMETERS
 * pixels:     the array which contains the pixels of the loaded image
 * pixelCount: the number of pixels in the loaded image
 */
void redMonochrome(PIXEL* pixels, long pixelCount)
{
    // loop through all the pixels in the image
    for(int pixel = 0; pixel < pixelCount; pixel++)
    {
        // make all the green and blue values of the pixel = 0
        pixels[pixel].g = 0;
        pixels[pixel].b = 0;
    }


}
/* This function adds a green filter to the image
 *
 * PARAMETERS
 * pixels:     the array which contains the pixels of the loaded image
 * pixelCount: the number of pixels in the loaded image
 */
void greenMonochrome(PIXEL* pixels, long pixelCount)
{
    // loop through all the pixels in the image
    for(int pixel = 0; pixel < pixelCount; pixel++)
    {
        // make all the red and blue values of the pixel = 0
        pixels[pixel].r = 0;
        pixels[pixel].b = 0;
    }
}
/* This function adds a blue filter to the image
 *
 * PARAMETERS
 * pixels:     the array which contains the pixels of the loaded image
 * pixelCount: the number of pixels in the loaded image
 */
void blueMonochrome(PIXEL* pixels, long pixelCount)
{
    // loop through all the pixels in the image
    for(int pixel = 0; pixel < pixelCount; pixel++)
    {
        // make all the red and green values of the pixel = 0
        pixels[pixel].r = 0;
        pixels[pixel].g = 0;
    }
}
/* This function changes the RGB values to the same value
 * using Y = 0.2126R + 0.7152G + 0.0722B
 *
 * PARAMETERS
 * pixels:     the array which contains the pixels of the loaded image
 * pixelCount: the number of pixels in the loaded image
 */
void greyScale(PIXEL* pixels, long pixelCount)
{
    // loop through all the pixels in the image
    for(int pixel = 0; pixel < pixelCount; pixel++)
    {
        // make all of the RGB values of the pixels the same value
        pixels[pixel].r = ((pixels[pixel].r*0.2126) + (pixels[pixel].g*0.7152) + (pixels[pixel].b*0.0722));
        pixels[pixel].g = pixels[pixel].r;
        pixels[pixel].b = pixels[pixel].r;
    }
}

/* This function rotates
 *
 * PARAMETERS
 * original:          pointer to array of pixels
 * pixelCount:        the number of pixels in the loaded image
 * height:            the height in pixels of the image
 * width:             the width of the image in pixels
 * directionRotated:  the direction the image is rotated e.g. clockwise or counter-clockwise
 * rataionAmount:     the amount the image is rotated in degrees
 */
void rotation( PIXEL* original, long pixelCount, uint& height, uint& width, int directionRotated, int rotationAmount)
{
    // creating a new instance of the array
    PIXEL* rotated = new PIXEL[pixelCount];

    switch(directionRotated)
    {
    case CLOCKWISE:
        switch(rotationAmount)
        {
            case 90:
            {
                // loop through every pixel changing its position
                for(uint y = 0; y < height; y++)
                {
                    for(uint x = 0; x < width; x++)
                    {
                        rotated[ width*height - height + y - x*height ] = original[ y*width + x ];
                    }
                }

                // setting the height value to the width
                uint temp = height;

                height = width;
                width = temp;
            }
            break;

            case 180:
                for(int y = 0; y < pixelCount; y++)
                {
                    rotated[pixelCount - y - 1] = original[y];
                }
            break;
        case 270:
            for(uint y = 0; y < height; y++)
            {
                for(uint x = 0; x < width; x++)
                {
                    rotated[ pixelCount - (width*height - height + y - x*height) - 1 ] = original[ y*width + x ];
                }
            }

            uint temp = width;
            width = height;
            height = temp;
            break;
        }
        break;
    case COUNTER_CLOCKWISE:
        switch(rotationAmount)
        {
            case 270:
            {
                for(uint y = 0; y < height; y++)
                {
                    for(uint x = 0; x < width; x++)
                    {
                        rotated[ width*height - height + y - x*height ] = original[ y*width + x ];
                    }
                }

                uint temp = height;

                height = width;
                width = temp;


            }
            break;

        case 180:
            for(int y = 0; y < pixelCount; y++)
            {
                rotated[pixelCount - y - 1] = original[y];
            }
            break;

        case 90:
            for(uint y = 0; y < height; y++)
            {
                for(uint x = 0; x < width; x++)
                {
                    rotated[ pixelCount - (width*height - height + y - x*height) - 1 ] = original[ y*width + x ];
                }
            }

            uint temp = width;
            width = height;
            height = temp;
            break;
        }
    }

    for(long i = 0; i < pixelCount; i++)
    {
        original[i] = rotated[i];
    }

    // clearing the memory used in the new instance
    delete[] rotated;
}

// returns true if extensio is bmp or tga
// otherwise returns false
bool hasValidFileExtension(string filename)
{
    if((getExtension(filename)== "bmp") || (getExtension(filename)== "tga"))
        return true;
    return false;
}

// returns the extension for a given filename e.g. bmp/tga/jpg
string getExtension(string pathName)
{
	// Finds the last dot character of the string
	int period = pathName.find_last_of(".");

	// take characters after the dot
	string extension = pathName.substr(period + 1).c_str();

	return extension;
}
